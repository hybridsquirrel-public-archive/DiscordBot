import os

import discord
from discord.ext import commands

client = commands.Bot(command_prefix='!')


@client.event
async def on_ready():
    await client.change_presence(status=discord.Status.idle, activity=discord.Game('Pink Floyd - Money'))
    print('Ready')


@client.command()
async def load(ctx, extenstion):
    client.load_extension(f'Cogs.{extenstion}')


@client.command()
async def unload(ctx, extenstion):
    client.unload_extension(f'Cogs.{extenstion}')


def main():
    for filename in os.listdir('./Cogs'):
        if filename.endswith('.py'):
            client.load_extension(f'Cogs.{filename[:-3]}')

    client.run('')


if __name__ == '__main__':
    main()
